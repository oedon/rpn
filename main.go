package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {

	stack := make(map[int]float64)

	i := 0
	for {
		var input string
		fmt.Printf("> ")
		fmt.Scan(&input)

		switch {
		case input == "+":
			i--
			fmt.Println(i)
			stack[i-1] = stack[i-1] + stack[i]

			delete(stack, i)

			for j := 0; j < len(stack); j++ {
				fmt.Printf("%d > %v\n", j, stack[j])
			}
		case input == "-":
			i--
			fmt.Println(i)
			stack[i-1] = stack[i-1] - stack[i]

			delete(stack, i)

			for j := 0; j < len(stack); j++ {
				fmt.Printf("%d > %v\n", j, stack[j])
			}
		case input == "/":
			i--
			fmt.Println(i)
			stack[i-1] = stack[i-1] / stack[i]

			delete(stack, i)

			for j := 0; j < len(stack); j++ {
				fmt.Printf("%d > %v\n", j, stack[j])
			}
		case input == "*":
			i--
			fmt.Println(i)
			stack[i-1] = stack[i-1] * stack[i]

			delete(stack, i)

			for j := 0; j < len(stack); j++ {
				fmt.Printf("%d > %v\n", j, stack[j])
			}
		case input == "pi":
			stack[i] = math.Pi
			i++

			for j := 0; j < len(stack); j++ {
				fmt.Printf("%d > %v\n", j, stack[j])
			}

		case input == "q":
			os.Exit(0)

		default:
			// Stack float check
			value, err := strconv.ParseFloat(input, 64)

			if err == nil {
				stack[i] = value
				i++

				for j := 0; j < len(stack); j++ {
					fmt.Printf("%d > %v\n", j, stack[j])
				}
			}

		}
	}

}
